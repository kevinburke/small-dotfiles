#!/bin/bash

set -e

mkdir -p ~/src

VERSION='5.0.5'
DIRECTORY="zsh-$VERSION"
TARBALL="$DIRECTORY.tar.gz"

if grep -iq "centos" /etc/issue; then
    yum -y install gcc ncurses-devel
fi

pushd ~/src
    if [ ! -f "$TARBALL" ]; then
        curl --location --output $TARBALL "http://www.zsh.org/pub/$TARBALL"
    fi
    if [ ! -d "$DIRECTORY" ]; then
        tar -xf "$TARBALL"
    fi
    pushd "$DIRECTORY"
        ./configure 
        make && make install
    popd
popd
