#!/bin/bash

set -ev

VERSION='1.6.2'
DIRECTORY="nginx-$VERSION"
BINARY="$DIRECTORY.tar.gz"

SUBS_DIRECTORY='ngx_http_substitutions_filter_module'

#/*if grep -qi centos /etc/issue; then*/
    #/*yum -y install zlib-devel perl-ExtUtils-MakeMaker gettext curl-devel*/
#/*fi*/
pushd ~/src
    if [ ! -f "$BINARY" ]; then 
        curl --location -O "http://nginx.org/download/$BINARY"
    fi
    if [ ! -d "$DIRECTORY" ]; then
        tar -xf "$BINARY"
    fi
    if [[ ! -d "$SUBS_DIRECTORY" ]]; then
        git clone git://github.com/yaoweibin/ngx_http_substitutions_filter_module.git
    fi
    pushd "$DIRECTORY"
        ./configure --prefix=$HOME --with-http_sub_module --with-http_ssl_module --with-pcre --add-module="$HOME/src/$SUBS_DIRECTORY"
        make && make install
        if [ $? -gt 0 ]; then
            exit 2
        fi
    popd
popd

