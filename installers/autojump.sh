#!/bin/sh

set -ev

pushd ~/code
    if [ ! -d autojump ]; then
        git clone git@github.com:joelthelion/autojump.git
    fi
    pushd autojump 
        ./install.py
    popd
popd
