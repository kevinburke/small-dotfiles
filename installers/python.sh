#!/bin/bash

set -e 

mkdir -p ~/src

VERSION='2.7.7'
DIRECTORY="Python-$VERSION"
TARBALL="$DIRECTORY.tgz"

if grep -qi centos /etc/issue; then
    yum -y install openssl-devel
fi
pushd ~/src
    if ! python --version 2>&1 | grep $VERSION; then 
        if [ ! -f $TARBALL ]; then
            curl --remote-name "https://www.python.org/ftp/python/$VERSION/Python-$VERSION.tgz"
        fi
        if [ ! -f $DIRECTORY ]; then
            tar -xf $TARBALL
        fi
        pushd $DIRECTORY
            ./configure
            make && make install
        popd
    fi

    # Pip
    if [ ! -f /usr/local/bin/pip ]; then
        curl --remote-name https://raw.github.com/pypa/pip/master/contrib/get-pip.py
        /usr/local/bin/python get-pip.py
    fi

    pip install virtualenv
popd

