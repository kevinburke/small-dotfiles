#!/bin/bash

set -eo pipefail

copy() {
    local launchd_folder="$1"
    local plist_name="$2"

    sudo cp $HOME/etc/focus.plist "$launchd_folder/$plist_name"
    sudo chown root:wheel "$launchd_folder/$plist_name"
    sudo launchctl unload "$launchd_folder/$plist_name"
    sudo launchctl load -w "$launchd_folder/$plist_name"
}

main() {
    local launchd_folder="$1"
    local plist_name="com.kevinburke.focus.plist"

    if [[ "$#" == 2 ]]; then
        local doit="$2"
    fi 
    # XXX there's a better check here
    if [[ -f "$launchd_folder/$plist_name" ]]; then
        if [[ "$doit" != "--go" ]]; then
            copy "$launchd_folder" "$plist_name"
        else
            echo "Not installing focus plist, already there"
        fi
    fi

    copy "$launchd_folder" "$plist_name"
}

main
