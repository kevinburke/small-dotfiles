#!/usr/bin/env bash

set -eo pipefail

main() {
    if [[ -f "$HOME/tmp/consolas/PowerPointViewer.exe" ]]; then
        return 0;
    fi
    brew install cabextract
    mkdir -p ~/tmp
    pushd ~/tmp
        mkdir -p consolas
        pushd consolas
            curl -O http://download.microsoft.com/download/f/5/a/f5a3df76-d856-4a61-a6bd-722f52a5be26/PowerPointViewer.exe
            cabextract PowerPointViewer.exe
            cabextract ppviewer.cab
            open CONSOL*.TTF
        popd
    popd
}

main "$@"
