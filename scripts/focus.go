package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"

	"github.com/kevinburke/hostsfile"
)

func checkError(err error) {
	if err != nil {
		log.Fatal(err.Error())
	}
}

// helper
func getPermissions(file *os.File) {
	fi, err := file.Stat()
	checkError(err)
	fmt.Println(fi.Mode())
}

func setAll(h *hostsfile.Hostsfile, url string) {
	local, err := net.ResolveIPAddr("ip", "127.0.0.1")
	checkError(err)

	ipv6local, err := net.ResolveIPAddr("ip", "fe80::1%lo0")
	checkError(err)

	h.Set(*local, url)
	h.Set(*ipv6local, url)
}

func main() {
	file, err := os.Open("/etc/hosts")
	checkError(err)
	h, err := hostsfile.Decode(file)
	checkError(err)
	setAll(&h, "gmail.com")
	setAll(&h, "mail.google.com")
	setAll(&h, "connect.facebook.net")
	setAll(&h, "www.facebook.com")
	setAll(&h, "facebook.com")
	setAll(&h, "apps.facebook.com")
	setAll(&h, "api.twitter.com")
	setAll(&h, "espn.go.com")
	setAll(&h, "lobste.rs")
	setAll(&h, "nytimes.com")
	setAll(&h, "www.nytimes.com")
	setAll(&h, "news.ycombinator.com")
	setAll(&h, "www.reddit.com")
	setAll(&h, "soccernet.com")
	setAll(&h, "www.instapaper.com")
	setAll(&h, "instapaper.com")
	setAll(&h, "www.espnfc.us")

	tmp, err := ioutil.TempFile("/tmp", "hostsfile-temp")
	checkError(err)

	err = hostsfile.Encode(tmp, h)
	checkError(err)

	err = os.Chmod(tmp.Name(), 0644)
	checkError(err)

	err = os.Rename(tmp.Name(), "/etc/hosts")
	checkError(err)
}
