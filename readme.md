# My dotfiles

Hope they're useful. I keep a lot of private things in .git/info/excludes,
which is not tracked by default.

## Install

### Downloading

If you have nothing on your computer the best option is for you to [download
the files directly via ZIP][download]. Cloning requires you to have an SSH
key added to bitbucket.org and a copy of the ssh config file used in this
repository.

Otherwise, you can run:

    git clone https://bitbucket.org/kevinburke/dotfiles.git

### Copying

Please be careful as this will overwrite any files in your current home
directory.

    cd
    cd dotfiles
    cp -na . ..

[download]: https://bitbucket.org/kevinburke/small-dotfiles/get/master.zip

Some of installation is automated; run `bash .kevin.install` (probably multiple
times) to complete installation.

## Submodules

To add a new submodule, run:

    git submodule add git@github.com:tpope/vim-surround.git cfg/nvim/bundle/surround

On one machine, do:

    git submodule foreach git pull origin master

This should update the commit history on each one. Then, on every other machine run:

    git submodule update

which will update them to the latest commit. You may possibly need to run

    git submodule update --init

to check out for the first time.

## Secret Keys

Follow the instructions here: https://www.keybits.net/post/import-keybase-private-key/

## New Go

Download Darwin .tar.gz source code from https://golang.org/dl/

mv ~/Downloads/go 2 ~/go1.9

## Generate GPG keys

Install GPG from Homebrew, then run:

    gpg --expert --full-generate-key

Choose (9) then (1) Curve 25519 at the prompts.

Then run

    gpg --list-secret-keys

to get the key ID (in the "sec" part). Then run:

    gpg --armor --export CE7043...

to get an exportable public key. Upload that last bit to Github.
