call pathogen#infect('~/cfg/nvim/bundle/{}')
syntax on

filetype plugin indent on

if &encoding != "utf-8"
    set encoding=utf-8
endif

set termguicolors

set modelines=0
highlight MatchParen ctermbg=4
set laststatus=2
"set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)
set statusline=%<%f\ (%{&ft})\ %-4(%m%)%=%-19(%3l,%02c%03V%)
set noerrorbells
set cursorline
set history=1000
set scrolloff=8
set sidescrolloff=5
set autoindent
set showmode
set splitright
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set wildignore+=*.hi,*.pyc,*.class,*.o,*.dSYM,*.dSYM/,*.egg-info,*.egg-info/,venv,cover,_build,include,Godeps,node_modules
set wildignorecase
set visualbell
set ruler
set backspace=indent,eol,start
set laststatus=2
set lazyredraw
set timeout timeoutlen=1000 ttimeoutlen=10
set nojoinspaces
set autoread

set wrap
set textwidth=80
" c = auto wrap comments on 80 line length
set formatoptions=cqrn1

"" search settings
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch

if version >= 703
    set undodir=~/cfg/nvim/tmp/undo/
    set backupdir=~/cfg/nvim/tmp/backup/
    set directory=~/cfg/nvim/tmp/swap/

    if has("signs")
        set relativenumber
    endif

    set colorcolumn=81
    set formatprg=par\ -w80\ -T4
    hi ColorColumn ctermbg=lightgrey guibg=#cccccc
endif

"" show line endings
set list
set listchars=tab:+\ ,eol:¬

" This doesn't work well, but I'm not sure why. 
" http://vi.stackexchange.com/a/6966/7848
tnoremap <Esc> <C-\><C-n>

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
inoremap jj <ESC>
imap <c-c> <esc>
cnoremap %% <C-R>=expand('%:h').'/'<cr>
map <leader>e :edit %%

nnoremap Q @='n.'<CR>

"" one less key for write
nnoremap ; :

"" matchit remapping
nmap <tab> %
vmap <tab> %

"" end of line
nnoremap L $
vnoremap L $

"" center after jump
nnoremap n nzz
nnoremap N Nzz

let mapleader = ","

"" clear a search
nnoremap <leader><space> :nohlsearch<cr>

nnoremap <leader>c gg"+yG<cr>
vnoremap <leader>c :CoffeeCompile<cr>

" flush commandt results
nnoremap <leader>f :CtrlPClearCache<cr>

function! GoSyntaxCheck()
    if (match(expand("%"), "test") != -1) 
        :GoTestCompile
    else
        :GoBuild -i
    endif
endfunction

nnoremap <leader>d :call GoSyntaxCheck()<CR>

set ttimeoutlen=50

if &term =~ "xterm" || &term =~ "screen"
  let g:CommandTCancelMap = ['<ESC>', '<C-c>']

  " when I originally started using Command-T inside a terminal,
  " I used to need these as well:
  let g:CommandTSelectNextMap = ['<C-j>', '<ESC>OB']
  let g:CommandTSelectPrevMap = ['<C-k>', '<ESC>OA']
endif

let g:go_test_timeout = '2s'
nnoremap <leader>g :GoTest<cr>

nnoremap <leader>r :!test-files-in-diff \| xargs tt<CR>
nnoremap <leader>n :call WriteAndRunTest()<CR>

let test#strategy = "neovim"
let g:test#preserve_screen = 1
" let test#javascript#mocha#executable = 'tt2 --bail'

let test#php#runner = './bin/run-tests'
let test#php#phpunit#executable = './bin/run-tests'


function! RunFile()
    if (&ft == 'python')
      exec ":!source venv/bin/activate; python " . bufname('%')
    endif
endfunction

function! WriteAndRunTest()
    " save the file first
    if expand("%") != ""
      if &ft=='go'
      else
        :w
      endif
    end
    if &ft=='go'
      :GoTestFunc
    else
      :TestNearest
    endif
endfunction

function! RunTest()
    if (&ft=='python')
        execute "normal" ?def test_<CR>wyw
    endif
endfunction

nnoremap <leader>o :call ReloadChrome()<CR>:pwd<cr>

"" visually select last edited text
nmap gV `[v`]

set background=dark
colorscheme spring-night

"to avoid crontab edit errors
set backupskip=/tmp/*,/private/tmp/*,*.zip


if has('cmdlog')
  " cmdlogdir:
  "     The directory which will be used to store logs.
  set cmdlogdir=~/.vimlogs/
  "" cmdloginsert:
  ""     Log text entered in normal mode.
  ""     Disabled by default
  set cmdloginsert
end

"" default tab settings - overridden for specific extensions below
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
if has("autocmd")
    augroup allfiles 
        autocmd BufWritePre * call StripEndingNewlines()
    augroup END

    augroup coffeescript
        autocmd BufRead,BufNewFile *.coffee setfiletype coffee
        autocmd FileType coffee set softtabstop=2
        autocmd FileType coffee set tabstop=2
        autocmd FileType coffee set shiftwidth=2
        autocmd BufWritePre *.coffee :call StripTrailingWhitespaces()
    augroup END

    augroup swift
        autocmd BufWritePre *.swift :call StripTrailingWhitespaces()
    augroup END

    augroup sh
        autocmd FileType sh set softtabstop=4
        autocmd FileType sh set tabstop=4
        autocmd FileType sh set shiftwidth=4
        autocmd BufWritePre *.sh :call StripTrailingWhitespaces()
    augroup END

    augroup makefile
        autocmd BufEnter Makefile set softtabstop=4
        autocmd BufEnter Makefile set tabstop=4
        autocmd BufEnter Makefile set shiftwidth=4
        autocmd BufWritePre Makefile :call StripTrailingWhitespaces()
    augroup END

    augroup javascript
        autocmd BufEnter *.js set softtabstop=2
        autocmd BufEnter *.js set tabstop=2
        autocmd BufEnter *.js set shiftwidth=2
        autocmd BufEnter *.js set expandtab
        autocmd BufWritePre *.js :call StripTrailingWhitespaces()
    augroup END

    augroup typescript
        autocmd FileType typescript set softtabstop=2
        autocmd FileType typescript set tabstop=2
        autocmd FileType typescript set shiftwidth=2
        autocmd FileType typescript set expandtab
        autocmd BufWritePre '*.ts' :call StripTrailingWhitespaces()
    augroup END

    augroup gitcommit
        autocmd FileType gitcommit set colorcolumn=71
        autocmd FileType gitcommit set formatprg=par\ -w70\ -T4
        autocmd FileType gitcommit set textwidth=70
        autocmd FileType gitcommit autocmd BufWritePre <buffer> :call StripTrailingWhitespaces()
    augroup END

    augroup proto
        autocmd BufRead,BufNewFile *.proto setfiletype proto
        autocmd BufRead,BufNewFile *.proto set expandtab
        autocmd BufWritePre *.proto :call StripTrailingWhitespaces()
        autocmd FileType proto setlocal shiftwidth=2 tabstop=2 softtabstop=2
    augroup END

    augroup golang
        autocmd BufEnter *.go set filetype=go
        autocmd FileType go setlocal tabstop=4 softtabstop=4 shiftwidth=4
        autocmd BufEnter *.go setlocal tabstop=4 softtabstop=4
        autocmd BufEnter *.go set noexpandtab
    augroup END

    augroup yml
        autocmd BufEnter *.yml set softtabstop=2
        autocmd BufEnter *.yml set tabstop=2
        autocmd BufEnter *.yml set shiftwidth=2
        autocmd BufWritePre *.yml call StripTrailingWhitespaces()
    augroup END

    augroup tfrm
        autocmd BufEnter *.tf set softtabstop=2
        autocmd BufEnter *.tf set tabstop=2
        autocmd BufEnter *.tf set shiftwidth=2

        autocmd BufRead,BufNewFile *.tf setfiletype hcl
    augroup END

    augroup css
        autocmd BufEnter *.css set softtabstop=2
        autocmd BufEnter *.css set tabstop=2
        autocmd BufEnter *.css set shiftwidth=2
        autocmd BufEnter *.css set expandtab

        autocmd BufWritePre *.css :call StripTrailingWhitespaces()
    augroup END

    augroup markdown
        autocmd BufEnter *.md,*.markdown set softtabstop=4
        autocmd BufEnter *.md,*.markdown set tabstop=4
        autocmd BufEnter *.md,*.markdown set shiftwidth=4
        autocmd BufEnter *.md,*.markdown set expandtab

        autocmd BufWritePre *.md,*.markdown :call StripTrailingWhitespaces()
    augroup END

    augroup jsx
        autocmd BufEnter *.jsx set softtabstop=2
        autocmd BufEnter *.jsx set tabstop=2
        autocmd BufEnter *.jsx set shiftwidth=2
        autocmd BufEnter *.jsx set expandtab

        autocmd BufWritePre *.jsx :call StripTrailingWhitespaces()
    augroup END

    augroup sql
        autocmd BufEnter *.sql set softtabstop=4
        autocmd BufEnter *.sql set tabstop=4
        autocmd BufEnter *.sql set shiftwidth=4
        autocmd BufEnter *.sql set expandtab

        autocmd BufWritePre *.sql :call StripTrailingWhitespaces()
    augroup END

    augroup c
        autocmd BufWritePre *.c :call StripTrailingWhitespaces()
    augroup END

    " wrap long lines in quickfix
    augroup quickfix
        autocmd!
        autocmd FileType qf setlocal wrap
    augroup END

    augroup bazel
        autocmd BufRead,BufNewFile *.bazel set ft=bzl syntax=bzl
    augroup END

    "" delete trailing whitespace when you save a file
    autocmd BufWritePre *.php :call StripTrailingWhitespaces()
    autocmd BufWritePre *.py :call StripTrailingWhitespaces()
    autocmd BufWritePre *.rb :call StripTrailingWhitespaces()
    autocmd BufWritePre *.txt :call StripTrailingWhitespaces()
    autocmd BufWritePre *.hs :call StripTrailingWhitespaces()
    autocmd BufWritePre *.rb :call StripTrailingWhitespaces()
    autocmd BufWritePre *.java :call StripTrailingWhitespaces()
    autocmd BufWritePre *.bash :call StripTrailingWhitespaces()
    autocmd BufWritePre *.json :call StripTrailingWhitespaces()
    autocmd BufWritePre *.html :call StripTrailingWhitespaces()
    autocmd BufWritePre *.conf :call StripTrailingWhitespaces()
    autocmd BufWritePre *.rules :call StripTrailingWhitespaces()

    " auto wrap .txt files to 80 characters
    autocmd BufRead,BufNewFile *.txt set formatoptions+=t

    autocmd BufRead,BufNewFile *.py,*.scss,*.conf,*.conf.disabled set expandtab

    autocmd BufRead,BufNewFile *.php.mock setfiletype php

    autocmd BufRead,BufNewFile *.mustache set ft=html syntax=html
    autocmd BufRead,BufNewFile *.phtml set ft=html syntax=html

    " detect text filetype
    autocmd BufEnter * if &filetype == "" | setlocal ft=txt | endif

    autocmd FileType nginx setlocal shiftwidth=4 tabstop=4 softtabstop=4

    autocmd FileType html set filetype=htmldjango
    autocmd FileType html set expandtab
    autocmd FileType mustache set filetype=htmldjango
    autocmd FileType mako set filetype=mako
    autocmd FileType modula2 set filetype=markdown

    autocmd FileType haml,html,ruby setlocal shiftwidth=2 tabstop=2 softtabstop=2

endif

let g:gist_post_private = 1
let g:go_fmt_command = "goimports"
let g:go_list_type = "quickfix"
let g:terraform_fmt_on_save = 1

function! StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

function! StripEndingNewlines()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    " remove ending whitespace: http://stackoverflow.com/a/7496112/329700, 
    " silent on error: http://stackoverflow.com/a/1043613/329700
    %s/\($\n\s*\)\+\%$//e
    let @/=_s
    call cursor(l, c)
endfunction

if has("gui_running")
    set guioptions=egmrt
endif

let CommandTMaxFiles = 40000

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_pylint_args ='--disable=line-too-long'

" https://github.com/fatih/vim-go#using-with-syntastic
let g:syntastic_disabled_filetypes=['go']
let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']
let g:syntastic_javascript_checkers = []
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }

let g:syntastic_javascript_checkers=['eslint']

let g:ctrlp_map = '<leader>t'
let g:ctrlp_cmd = 'CtrlP'
map <leader>b :CtrlPBuffer<CR>
let g:ctrlp_working_path_mode = 'ra'

" https://github.com/neovim/neovim/issues/2048
if has('nvim')
    nmap <BS> <C-W>h
endif
