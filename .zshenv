## pyenv
if [[ -d "/usr/local/opt/pyenv" ]]; then
    export PYENV_ROOT="/usr/local/opt/pyenv"
fi

export EDITOR=$(which vim)

fignore=($FIGNORE .hi .pyc .egg-info .o .beam .dSYM .un~ __pycache__)

export WORKON_HOME=~/.envs

HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.history

