#!/bin/bash

# Test whether your terminal supports the following ansi escape sequences
for i in {1..50}; do
    printf '%d \e[0;%dmfoobarblahbizbang\033[0m\n' $i $i;
done
